Write Springboot application for calculate covid case
==========================================================================
**Information**
```
  This project for coding interview about create application to get data from public api 
and calculate date to get result follow expect result on topic 
"Design & Create API below this topic and handle exception follow unit test case"
```

**Technical skill**
```
- Using REST Architecture
- Spring boot
- Webclient (spring-boot-starter-webflux)
- Spring MVC
- Data H2 (Optional)
- Sprint security (Optional)
- JWT token (Optional)
- Unit test
- Exception Handling
    - Success
    - Fail
```
**How to develop code**
```
  Please develop code follow guild line in project
  - TODO : Require to develop and finish task
  - TODO <Optional>  : Not require to finish 
  ** If you can do optional task ,We will take special consideration.
```

**How to submit your code to review**
```
  Please using gitflow Workflow by create pull request to develop with your firstname_lastname
```

API Guildline to get data for calculate response
==========================================================================
- https://covid19.ddc.moph.go.th/api/Cases/today-cases-all
- https://covid19.ddc.moph.go.th/api/Cases/today-cases-by-provinces

Design & Create API below this topic and handle exception follow unit test case
==========================================================================
- **Calculate find maximum case compare with all province**

  _Expect response :_
  ```JSON
  {
   "province" : "กรุงเทพมหานคร",
   "percentage" : "30"
  }
  ```

- **Calculate percentage of province compare with total case**

  _Expect response :_
  ```JSON
  {
   "province" : "กรุงเทพมหานคร",
   "percentage" : "30"
  }
  ```

- **Authenticate with jwt token (Optional)**

  _Expect request :_
  ```JSON
  {
   "username":"{username}",
   "password":"{password}"
  }
  ```
  _Expect response :_
  ```JSON
  {
   "token":"{jwt_token}"
  }
  ```

- **Modify all api validate jwt token & expire jwt token (Optional)**
- **Validate request escape special character (Optional)**
	
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		