package com.tmn.interview.client.ddc.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.UtilityClass;

public class Covid19DdcModel {


    @UtilityClass
    public static class GetDataTodayCasesByProvinces{
        @Setter
        @Getter
        @JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
        @Builder
        public static class Response {

        }

    }

    @UtilityClass
    public static class GetTodayCasesAll{
        @Setter
        @Getter
        @JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
        @Builder
        public static class Response {


        }

    }
}
