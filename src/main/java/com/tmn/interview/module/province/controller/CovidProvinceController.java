package com.tmn.interview.module.province.controller;


import com.tmn.interview.module.ApiRoot;
import com.tmn.interview.module.province.model.CovidProvinceModel;
import com.tmn.interview.module.province.service.CovidProvinceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequiredArgsConstructor
@Log4j2
public class CovidProvinceController implements ApiRoot {

    private final CovidProvinceService covidProvinceService;

    @GetMapping("/covid/stat/province")
    public CovidProvinceModel.GetCovidStatByProvince.Response getCovidStatByProvince(@RequestParam(value = "name") String proviceName) {
        //TODO - add validation request when null request parameter and escape special character in parameter

        return covidProvinceService.getCovidStatByProvince(proviceName);
    }


}
