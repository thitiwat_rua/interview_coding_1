package com.tmn.interview.module.province.service;

import com.tmn.interview.client.ddc.Covid19DdcClient;
import com.tmn.interview.module.province.model.CovidProvinceModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Log4j2
@Service
@RequiredArgsConstructor
public class CovidProvinceService {

    private final Covid19DdcClient covid19DdcClient;

    public CovidProvinceModel.GetCovidStatByProvince.Response getCovidStatByProvince(String provinceName) {
        //TODO - add logic for get data from API guildline in README.md to calculate response and Handle exception follow unit test case

        return CovidProvinceModel.GetCovidStatByProvince.Response.builder().build();
    }
}
