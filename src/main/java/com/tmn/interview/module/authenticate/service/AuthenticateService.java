package com.tmn.interview.module.authenticate.service;


import com.tmn.interview.module.authenticate.model.AuthenticateModel;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class AuthenticateService {

    public AuthenticateModel.Response authenticate(AuthenticateModel.Request request){
        //TODO <Optional>
        // - add logic for authenticate using h2 database and create table for save username/password
        // - add logic to generate jwt token with expire time in 15 minute

        return AuthenticateModel.Response.builder().build();
    }
}
