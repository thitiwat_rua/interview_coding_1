package com.tmn.interview.module.authenticate.controller;


import com.tmn.interview.module.ApiRoot;
import com.tmn.interview.module.authenticate.model.AuthenticateModel;
import com.tmn.interview.module.authenticate.service.AuthenticateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
@RequiredArgsConstructor
@Log4j2
public class AuthenticateController implements ApiRoot {
    private final AuthenticateService authenticateService;



    @PostMapping("/auth")
    public AuthenticateModel.Response authenticate (@RequestBody AuthenticateModel.Request request){
        //TODO <Optional> - add validation request when null request parameter and escape special character in parameter

        return authenticateService.authenticate(request);
    }
}
