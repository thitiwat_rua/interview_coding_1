package com.tmn.interview.module.stat.controller;

import com.tmn.interview.exception.ExceptionHandling;
import com.tmn.interview.module.province.controller.CovidProvinceController;
import com.tmn.interview.module.stat.service.CovidStatService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@ExtendWith({MockitoExtension.class})
public class CovidStatControllerTest {

    private MockMvc mockMvc;

    @Mock
    private CovidStatService covidStatService;

    @BeforeEach
    void beforeEach() {
        mockMvc = standaloneSetup(new CovidStatController(covidStatService))
                .setControllerAdvice(new ExceptionHandling())
                .build();
    }

    @Test
    void getCovidStatMaximumProvince_Success() {
        //TODO - Create unit test for call api success

    }

    @Test
    void getCovidStatMaximumProvince_Fail_Connection_Error() {
        //TODO - Create unit test for call api fail connection error
    }

    @Test
    void getCovidStatMaximumProvince_Fail_Connection_TimeOut() {
        //TODO - Create unit test for call api fail connection timeout
    }

    @Test
    void getCovidStatMaximumProvince_Fail_System_Error() {
        //TODO - Create unit test for call api fail system error
    }

}
