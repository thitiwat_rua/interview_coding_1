package com.tmn.interview.module.authenticate.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith({MockitoExtension.class})
class AuthenticateServiceTest {

    private AuthenticateService authenticateService;


    @BeforeEach
    void beforeEach(){
        authenticateService = new AuthenticateService();
    }

    @Test
    void authenticate_Success() {
        //TODO <Optional> - Create unit test for authenticate success get token from api
    }


    @Test
    void authenticate_Fail_Invalid_Password() {
        //TODO <Optional> - Create unit test for authenticate fail invalid password

    }

    @Test
    void authenticate_Fail_Null_Username() {
        //TODO <Optional> - Create unit test for authenticate fail null parameter username

    }

    @Test
    void authenticate_Fail_Null_Password() {
        //TODO <Optional> - Create unit test for authenticate fail null parameter password

    }

    @Test
    void authenticate_Fail_System_Error() {
        //TODO <Optional> - Create unit test for authenticate fail system error

    }

}
